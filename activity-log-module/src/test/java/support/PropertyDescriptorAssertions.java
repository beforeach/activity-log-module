package support;

import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.ViewElementLookupRegistry;
import com.foreach.across.modules.entity.views.ViewElementMode;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class PropertyDescriptorAssertions
{
	private final EntityPropertyDescriptor entityPropertyDescriptor;

	public static PropertyDescriptorAssertions assertPropertyDescriptor( EntityPropertyDescriptor entityPropertyDescriptor ) {
		return new PropertyDescriptorAssertions( entityPropertyDescriptor );
	}

	public PropertyDescriptorAssertions hasParentDescriptor( EntityPropertyDescriptor parent ) {
		assertThat( entityPropertyDescriptor.getParentDescriptor() ).isEqualTo( parent );
		return this;
	}

	public PropertyDescriptorAssertions hasPropertyType( Class expected ) {
		assertThat( entityPropertyDescriptor.getPropertyType() ).isEqualTo( expected );
		return this;
	}

	public PropertyDescriptorAssertions hasPropertyTypeDescriptor( TypeDescriptor expected ) {
		assertThat( entityPropertyDescriptor.getPropertyTypeDescriptor() ).isEqualTo( expected );
		return this;
	}

	public PropertyDescriptorAssertions hasViewElementType( ViewElementMode mode, String type ) {
		assertThat( entityPropertyDescriptor.hasAttribute( ViewElementLookupRegistry.class ) ).isTrue();
		assertThat( entityPropertyDescriptor.getAttribute( ViewElementLookupRegistry.class ).getViewElementType( mode ) ).isEqualTo( type );
		return this;
	}

	public PropertyDescriptorAssertions isEmbedded() {
		assertThat( entityPropertyDescriptor.getAttribute( EntityAttributes.IS_EMBEDDED_OBJECT ) ).isEqualTo( true );
		assertThat( entityPropertyDescriptor.getAttribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR ) ).isEqualTo( EntityPropertySelector.all() );
		return this;
	}

	public PropertyDescriptorAssertions fetchesValueViaParent() {
		assertThat( entityPropertyDescriptor.getParentDescriptor() ).isNotNull();
		assertThat( entityPropertyDescriptor.getParentDescriptor().getValueFetcher() ).isNotNull();
		return this;
	}

	public PropertyDescriptorAssertions fetchesValueDirectly() {
		assertThat( entityPropertyDescriptor.getParentDescriptor() ).isNull();
		return this;
	}

	public PropertyDescriptorAssertions fetchesValue( Object expectedValue ) {
		assertThat( entityPropertyDescriptor.getValueFetcher().getValue( new Object() ) )
				.isEqualTo( expectedValue );
		return this;
	}
}
