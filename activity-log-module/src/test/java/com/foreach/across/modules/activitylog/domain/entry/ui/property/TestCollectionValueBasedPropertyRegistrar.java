package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.entity.registry.properties.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.TypeDescriptor;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static support.PropertyDescriptorAssertions.assertPropertyDescriptor;

public class TestCollectionValueBasedPropertyRegistrar
{
	private PropertyValueBasedRegistrar propertyValueBasedRegistrar;

	@BeforeEach
	void setUp() {
		PropertyValueBasedRegistryProvider propertyValueBasedRegistryProvider = new PropertyValueBasedRegistryProvider();
		propertyValueBasedRegistryProvider.setPostProcessors( Collections.singletonList( new StringValueBasedPropertyRegistrar() ) );
		propertyValueBasedRegistrar = new CollectionValueBasedPropertyRegistrar( propertyValueBasedRegistryProvider );
	}

	@Test
	void createWithNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Collection<String> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "names", expectedValue, propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( Collection.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.collection( Collection.class, TypeDescriptor.valueOf( String.class ) ) )
				.fetchesValueDirectly()
				.fetchesValue( expectedValue );
	}

	@Test
	void createWithNonNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();

		MutableEntityPropertyDescriptor parentDescriptor = new SimpleEntityPropertyDescriptor( "parent" );
		Map<String, Object> parentValue = new HashMap<>();
		parentValue.put( "names", Collections.singletonList( "Johnson" ) );
		parentDescriptor.setValueFetcher( ( obj ) -> parentValue );

		Collection<String> expectedValue = getSerializedValue();

		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( parentDescriptor, "names", expectedValue,
		                                                                                                   propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( Collection.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.collection( Collection.class, TypeDescriptor.valueOf( String.class ) ) )
				.hasParentDescriptor( parentDescriptor )
				.fetchesValueViaParent()
				.fetchesValue( Collections.singletonList( "Johnson" ) );
	}

	@Test
	void createMemberDescriptors() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Collection<String> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "names", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
	}

	@Test
	void noMemberDescriptorsAreCreated() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Collection<String> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "names", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
		propertyRegistry.register( descriptor );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "names" ) );

		EntityPropertyRegistry fetchedPropertyRegistry = descriptor.getPropertyRegistry();
		assertThat( fetchedPropertyRegistry ).isNotNull()
		                                     .isEqualTo( propertyRegistry );

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNotNull()
		                                              .isEqualTo( fetchedPropertyRegistry );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "names" ) );
	}

	// todo add tests for nested maps (& collection of dates?)

	private Collection<String> getSerializedValue() {
		return Arrays.asList( "Jonas", "Freddy", "Joanna" );
	}
}
