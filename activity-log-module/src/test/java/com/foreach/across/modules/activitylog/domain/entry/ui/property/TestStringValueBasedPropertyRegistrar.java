package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.entity.registry.properties.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.TypeDescriptor;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static support.PropertyDescriptorAssertions.assertPropertyDescriptor;

public class TestStringValueBasedPropertyRegistrar
{
	private PropertyValueBasedRegistrar propertyValueBasedRegistrar = new StringValueBasedPropertyRegistrar();

	@Test
	void createWithNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "username", expectedValue, propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( String.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
				.fetchesValueDirectly()
				.fetchesValue( expectedValue );
	}

	@Test
	void createWithNonNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();

		MutableEntityPropertyDescriptor parentDescriptor = new SimpleEntityPropertyDescriptor( "parent" );
		Map<String, Object> parentValue = new HashMap<>();
		parentValue.put( "username", "joske" );
		parentDescriptor.setValueFetcher( ( obj ) -> parentValue );

		String expectedValue = getSerializedValue();

		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( parentDescriptor, "username", expectedValue,
		                                                                                                   propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( String.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( String.class ) )
				.hasParentDescriptor( parentDescriptor )
				.fetchesValueViaParent()
				.fetchesValue( "joske" );
	}

	@Test
	void createMemberDescriptors() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "username", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
	}

	@Test
	void noMemberDescriptorsAreCreated() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "username", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
		propertyRegistry.register( descriptor );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "username" ) );

		EntityPropertyRegistry fetchedPropertyRegistry = descriptor.getPropertyRegistry();
		assertThat( fetchedPropertyRegistry ).isNotNull()
		                                     .isEqualTo( propertyRegistry );

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNotNull()
		                                              .isEqualTo( fetchedPropertyRegistry );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "username" ) );
	}

	private String getSerializedValue() {
		return "Jos Leemans";
	}
}
