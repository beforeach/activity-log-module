package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.entity.registry.properties.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.TypeDescriptor;

import java.time.*;
import java.time.temporal.Temporal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static support.PropertyDescriptorAssertions.assertPropertyDescriptor;

public class TestDateValueBasedPropertyRegistrar
{
	private PropertyValueBasedRegistrar propertyValueBasedRegistrar = new DateValueBasedPropertyRegistrar();

	@Test
	void createWithNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "dateOfBirth", expectedValue,
		                                                                                                   propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( LocalDate.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( LocalDate.class ) )
				.fetchesValueDirectly()
				.fetchesValue( LocalDate.of( 2019, 5, 11 ) );
	}

	@Test
	void createWithNonNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();

		MutableEntityPropertyDescriptor parentDescriptor = new SimpleEntityPropertyDescriptor( "parent" );
		Map<String, Object> parentValue = new HashMap<>();
		parentValue.put( "dateOfBirth", LocalDate.of( 2000, 3, 11 ) );
		parentDescriptor.setValueFetcher( ( obj ) -> parentValue );

		String expectedValue = getSerializedValue();

		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( parentDescriptor, "dateOfBirth", expectedValue,
		                                                                                                   propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( LocalDate.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( LocalDate.class ) )
				.hasParentDescriptor( parentDescriptor )
				.fetchesValueViaParent()
				.fetchesValue( LocalDate.of( 2000, 3, 11 ) );
	}

	@Test
	void createMemberDescriptors() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "dateOfBirth", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
	}

	@Test
	void noMemberDescriptorsAreCreated() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		String expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "dateOfBirth", expectedValue,
		                                                                                                   propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
		propertyRegistry.register( descriptor );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "dateOfBirth" ) );

		EntityPropertyRegistry fetchedPropertyRegistry = descriptor.getPropertyRegistry();
		assertThat( fetchedPropertyRegistry ).isNotNull()
		                                     .isEqualTo( propertyRegistry );

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNotNull()
		                                              .isEqualTo( fetchedPropertyRegistry );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "dateOfBirth" ) );
	}

	@Test
	void supportsTemporalTypes() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Arrays.asList(
				TemporalHolder.of( "2007-12-03T10:15:30+01:00[Europe/Paris]", ZonedDateTime::parse, ZonedDateTime.class ),
				TemporalHolder.of( "2007-12-03T10:15:30+01:00", ZonedDateTime::parse, ZonedDateTime.class ),
				TemporalHolder.of( "10:15:30+01:00", OffsetTime::parse, OffsetTime.class ),
				TemporalHolder.of( "2007-12-03T10:15:30", LocalDateTime::parse, LocalDateTime.class ),
				TemporalHolder.of( "2007-12-03", LocalDate::parse, LocalDate.class ),
				TemporalHolder.of( "10:15:30", LocalTime::parse, LocalTime.class ),
				TemporalHolder.of( "2011-12-03T10:15:30Z", ZonedDateTime::parse, ZonedDateTime.class )
		).forEach( holder -> {
			MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "dateOfBirth", holder.getSerializedValue(),
			                                                                                                   propertyRegistry );
			assertPropertyDescriptor( descriptor )
					.hasPropertyType( holder.getType() )
					.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( holder.getType() ) )
					.fetchesValueDirectly()
					.fetchesValue( holder.getMapper().apply( holder.getSerializedValue() ) );
		} );

	}

	private String getSerializedValue() {
		return "2019-05-11";
	}

	@Getter
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	static class TemporalHolder
	{
		private final String serializedValue;
		private final Function<String, Temporal> mapper;
		private final Class<? extends Temporal> type;

		static TemporalHolder of( String serialized, Function<String, Temporal> mapper, Class<? extends Temporal> type ) {
			return new TemporalHolder( serialized, mapper, type );
		}
	}
}
