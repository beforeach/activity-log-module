package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.entity.registry.properties.*;
import com.foreach.across.modules.entity.views.ViewElementMode;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.TypeDescriptor;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static support.PropertyDescriptorAssertions.assertPropertyDescriptor;

@ExtendWith(MockitoExtension.class)
public class TestMapValueBasedPropertyRegistrar
{
	private PropertyValueBasedRegistrar propertyValueBasedRegistrar;

	@BeforeEach
	void setUp() {
		PropertyValueBasedRegistryProvider propertyValueBasedRegistryProvider = new PropertyValueBasedRegistryProvider();
		propertyValueBasedRegistryProvider.setPostProcessors( Collections.singletonList( new StringValueBasedPropertyRegistrar() ) );
		propertyValueBasedRegistrar = new MapValueBasedPropertyRegistrar( propertyValueBasedRegistryProvider,
		                                                                  DefaultEntityPropertyRegistryProvider.newInstance() );
	}

	@Test
	void createWithNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Map<String, Object> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "user", expectedValue, propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( Object.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( Object.class ) )
				.hasViewElementType( ViewElementMode.FORM_READ, BootstrapUiElements.FIELDSET )
				.isEmbedded()
				.fetchesValueDirectly()
				.fetchesValue( expectedValue );
	}

	@Test
	void createWithNonNullParent() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();

		MutableEntityPropertyDescriptor parentDescriptor = new SimpleEntityPropertyDescriptor( "parent" );
		Map<String, Object> parentValue = new HashMap<>();
		parentValue.put( "user", "joske" );
		parentDescriptor.setValueFetcher( ( obj ) -> parentValue );

		Map<String, Object> expectedValue = getSerializedValue();

		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( parentDescriptor, "user", expectedValue,
		                                                                                                   propertyRegistry );

		assertPropertyDescriptor( descriptor )
				.hasPropertyType( Object.class )
				.hasPropertyTypeDescriptor( TypeDescriptor.valueOf( Object.class ) )
				.hasViewElementType( ViewElementMode.FORM_READ, BootstrapUiElements.FIELDSET )
				.hasParentDescriptor( parentDescriptor )
				.isEmbedded()
				.fetchesValueViaParent()
				.fetchesValue( "joske" );
	}

	@Test
	void createMemberDescriptors() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Map<String, Object> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "user", expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );
		EntityPropertyRegistry fetchedPropertyRegistry = descriptor.getPropertyRegistry();
		assertThat( fetchedPropertyRegistry ).isNotNull();

		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();
		assertThat( fetchedPropertyRegistry.getRegisteredDescriptors() )
				.hasSize( 3 )
				.anyMatch( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "name" ) )
				.anyMatch( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "lastName" ) )
				.anyMatch( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "dateOfBirth" ) );
	}

	@Test
	void propertyRegistryIsOverriddenAfterRegistrationOfMembers() {
		MutableEntityPropertyRegistry propertyRegistry = new DefaultEntityPropertyRegistry();
		Map<String, Object> expectedValue = getSerializedValue();
		MutableEntityPropertyDescriptor descriptor = propertyValueBasedRegistrar.createPropertyDescriptor( null, "user", expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNull();
		assertThat( propertyRegistry.getRegisteredDescriptors() ).isEmpty();

		propertyRegistry.register( descriptor );
		assertThat( propertyRegistry.getRegisteredDescriptors() )
				.isNotEmpty()
				.hasSize( 1 )
				.first()
				.matches( propertyDescriptor -> StringUtils.equals( propertyDescriptor.getName(), "user" ) );

		EntityPropertyRegistry fetchedPropertyRegistry = descriptor.getPropertyRegistry();
		assertThat( fetchedPropertyRegistry ).isNotNull()
		                                     .isEqualTo( propertyRegistry );

		propertyValueBasedRegistrar.registerMemberDescriptors( descriptor, expectedValue, propertyRegistry );

		assertThat( descriptor.getPropertyRegistry() ).isNotNull()
		                                              .isNotEqualTo( fetchedPropertyRegistry );

	}

	private Map<String, Object> getSerializedValue() {
		Map<String, Object> user = new HashMap<>();
		user.put( "name", "Jos" );
		user.put( "lastName", "Leemans" );
		user.put( "dateOfBirth", "2001-03-05" );
		return user;
	}
}
