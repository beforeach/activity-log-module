package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * {@link PropertyValueBasedRegistrar} that supports creating property descriptors for {@link Collection} types.
 * If the collection contains {@link Map} instances, an additional {@link EntityPropertyRegistry#INDEXER} property is created.
 */
@Component
@RequiredArgsConstructor
@OrderInModule(20000)
public class CollectionValueBasedPropertyRegistrar extends BasePropertyValueBasedRegistrar
{
	private final PropertyValueBasedRegistryProvider propertyValueBasedRegistryProvider;

	@Override
	public boolean handles( Object value ) {
		return value instanceof Collection;
	}

	@Override
	public MutableEntityPropertyDescriptor createPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                                                 String propertyName,
	                                                                 Object value,
	                                                                 MutableEntityPropertyRegistry propertyRegistry ) {
		Collection<Object> asCollection = (Collection<Object>) value;
		Optional<Object> firstNonNullElement = asCollection.stream()
		                                                   .filter( Objects::nonNull )
		                                                   .findFirst();

		EntityPropertyDescriptorBuilder descriptorBuilder =
				createBasePropertyDescriptorBuilder( parentDescriptor, propertyName )
						.controller( controller -> controller.valueFetcher( mapPropertyValueFetcher( parentDescriptor, propertyName, value ) ) )
						.propertyType( Collection.class );

		if ( firstNonNullElement.isPresent() && firstNonNullElement.get() instanceof Map ) {
			descriptorBuilder.propertyType( TypeDescriptor.collection( Collection.class, TypeDescriptor.valueOf( Map.class ) ) );
		}
		else {
			descriptorBuilder.propertyType( TypeDescriptor.collection( Collection.class, TypeDescriptor.valueOf( String.class ) ) );
		}

		return descriptorBuilder.build();
	}

	@Override
	public void registerMemberDescriptors( MutableEntityPropertyDescriptor collectionDescriptor,
	                                       Object collection,
	                                       MutableEntityPropertyRegistry propertyRegistry ) {
		Collection<Object> asCollection = (Collection<Object>) collection;
		Optional<Object> firstNonNullElement = asCollection.stream()
		                                                   .filter( Objects::nonNull )
		                                                   .findFirst();
		if ( firstNonNullElement.isPresent() ) {
			Object collectionItem = firstNonNullElement.get();
			if ( collectionItem instanceof Map ) {
				handleNestedMapDescriptor( collectionDescriptor, firstNonNullElement, propertyRegistry );
			}
		}
	}

	private void handleNestedMapDescriptor( MutableEntityPropertyDescriptor collectionDescriptor,
	                                        Optional<Object> firstNonNullValue,
	                                        MutableEntityPropertyRegistry propertyRegistry ) {
		Map<String, Object> collectionItem = (Map<String, Object>) firstNonNullValue.get();
		EntityPropertyDescriptorBuilder memberDescriptorBuilder =
				new EntityPropertyDescriptorBuilder( collectionDescriptor.getName() + EntityPropertyRegistry.INDEXER )
						.readable( true )
						.writable( false )
						.hidden( false )
						.parent( collectionDescriptor )
						.propertyType( Map.class )
						.propertyType( TypeDescriptor.valueOf( Map.class ) );

		MutableEntityPropertyDescriptor memberDescriptor = memberDescriptorBuilder.build();
		propertyRegistry.register( memberDescriptor );
		collectionItem.forEach( ( propertyName, propertyValue ) -> propertyValueBasedRegistryProvider
				.registerPropertyDescriptor( memberDescriptor, propertyName, propertyValue, propertyRegistry ) );
	}

}
