package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

/**
 * {@link PropertyValueBasedRegistrar} that supports creating an {@link com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor}
 * with a {@link String} type.
 */
@Component
@OrderInModule
public class StringValueBasedPropertyRegistrar extends BasePropertyValueBasedRegistrar
{
	@Override
	public boolean handles( Object value ) {
		return true;
	}

	@Override
	public MutableEntityPropertyDescriptor createPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                                                 String propertyName,
	                                                                 Object value,
	                                                                 MutableEntityPropertyRegistry propertyRegistry ) {
		EntityPropertyDescriptorBuilder descriptorBuilder =
				createBasePropertyDescriptorBuilder( parentDescriptor, propertyName )
						.propertyType( String.class )
						.propertyType( TypeDescriptor.valueOf( String.class ) )
						.controller( controller -> controller.valueFetcher( mapPropertyValueFetcher( parentDescriptor, propertyName, value ) ) );

		return descriptorBuilder.build();
	}
}
