package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;

import java.util.Map;
import java.util.function.Function;

public abstract class BasePropertyValueBasedRegistrar implements PropertyValueBasedRegistrar
{
	/**
	 * Utility method to create an initial {@link EntityPropertyDescriptorBuilder}.
	 * A nested descriptor is made if a parent {@link EntityPropertyDescriptor} is given which is not an embedded object or fielset.
	 * By default, properties created through this utility method are readable, but not writable.
	 * </p>
	 * If a parent descriptor is an embedded object or fieldset, then the nested properties are <strong>not</strong> prefixed by their parent property.
	 *
	 * @param parentDescriptor for the new descriptor that should be created
	 * @param propertyName     for which a new {@link EntityPropertyDescriptorBuilder} should be created
	 * @return an {@link EntityPropertyDescriptorBuilder} with default configuration.
	 */
	protected EntityPropertyDescriptorBuilder createBasePropertyDescriptorBuilder( EntityPropertyDescriptor parentDescriptor,
	                                                                               String propertyName ) {
		String fullPropertyName;
		// todo part of the hack to check for the top level parentDescriptor
		if ( parentDescriptor != null
				&& !Boolean.TRUE.equals( parentDescriptor.getAttribute( EntityAttributes.IS_EMBEDDED_OBJECT ) )
				&& parentDescriptor.hasAttribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR ) ) {
			fullPropertyName = parentDescriptor.getName() + "." + propertyName;
		}
		else {
			fullPropertyName = propertyName;
		}

		return new EntityPropertyDescriptorBuilder( fullPropertyName )
				.parent( parentDescriptor )
				.readable( true )
				.writable( false )
				.hidden( false );
	}

	/**
	 * Creates a valuefetcher that retrieves the value based on a given property name from the parent descriptor.
	 *
	 * @param parentDescriptor that holds the {@link Map} as it s value
	 * @param propertyName     key that should be used to retrieve the value from the map
	 * @param currentValue     fallback value in case the value can not be fetched via the parent descriptor
	 * @return a value fetcher that retrieves the value for the given key
	 */
	protected Function<Object, Object> mapPropertyValueFetcher( MutableEntityPropertyDescriptor parentDescriptor,
	                                                            String propertyName,
	                                                            Object currentValue ) {
		return ctx -> {
			if ( parentDescriptor == null || parentDescriptor.getValueFetcher() == null ) {
				return currentValue;
			}
			Map<String, Object> parentValue = (Map<String, Object>) parentDescriptor.getValueFetcher().getValue( ctx );
			return parentValue.get( propertyName );
		};
	}

	@Override
	public void registerMemberDescriptors( MutableEntityPropertyDescriptor valueDescriptor, Object value, MutableEntityPropertyRegistry propertyRegistry ) {

	}
}
