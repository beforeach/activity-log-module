package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.bootstrapui.elements.FieldsetFormElement;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.*;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.elements.ViewElementFieldset;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * {@link PropertyValueBasedRegistrar} that supports creating an {@link com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor}
 * for a {@link Map} value. Creates additional descriptors for each key in the map through the {@link PropertyValueBasedRegistryProvider}.
 * </p>
 * Instead of nested property descriptors, a new {@link EntityPropertyRegistry} will be created for the additional descriptors.
 */
@Component
@RequiredArgsConstructor
@OrderInModule(10000)
public class MapValueBasedPropertyRegistrar extends BasePropertyValueBasedRegistrar
{
	private final PropertyValueBasedRegistryProvider propertyValueBasedRegistryProvider;
	private final EntityPropertyRegistryProvider entityPropertyRegistryProvider;

	@Override
	public boolean handles( Object value ) {
		return value instanceof Map;
	}

	@Override
	public MutableEntityPropertyDescriptor createPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                                                 String propertyName,
	                                                                 Object value,
	                                                                 MutableEntityPropertyRegistry propertyRegistry ) {
		EntityPropertyDescriptorBuilder descriptorBuilder =
				createBasePropertyDescriptorBuilder( parentDescriptor, propertyName )
						.controller( controller -> controller.valueFetcher( mapPropertyValueFetcher( parentDescriptor, propertyName, value ) ) )
						.propertyType( Object.class )
						.propertyType( TypeDescriptor.valueOf( Object.class ) )
						.viewElementType( ViewElementMode.FORM_READ, BootstrapUiElements.FIELDSET )
						.attribute( EntityAttributes.IS_EMBEDDED_OBJECT, true )
						.attribute( FieldsetFormElement.ELEMENT_TYPE, ViewElementFieldset.TEMPLATE_FIELDSET )
						.attribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR, EntityPropertySelector.all() );

		return descriptorBuilder.build();
	}

	@Override
	public void registerMemberDescriptors( MutableEntityPropertyDescriptor mapDescriptor, Object value, MutableEntityPropertyRegistry propertyRegistry ) {
		DefaultEntityPropertyRegistry contentPropertyRegistry = new DefaultEntityPropertyRegistry( entityPropertyRegistryProvider );
		mapDescriptor.setPropertyRegistry( contentPropertyRegistry );
		Map<String, Object> asMap = (Map<String, Object>) value;
		asMap.forEach(
				( propertyName, propertyValue ) ->
						propertyValueBasedRegistryProvider
								.registerPropertyDescriptor( mapDescriptor, propertyName, propertyValue, contentPropertyRegistry ) );
		;
	}
}
