package com.foreach.across.modules.activitylog.domain.entry;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface ActivityLogEntryRepository extends IdBasedEntityJpaRepository<ActivityLogEntry>, QuerydslPredicateExecutor<ActivityLogEntry>
{
	Optional<ActivityLogEntry> findFirstByObjectTypeAndObjectIdOrderByVersionDesc( String objectType, String objectId );
}
