package com.foreach.across.modules.activitylog.extensions;

import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.activitylog.domain.ActivityLogDomainConfiguration;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@ModuleConfiguration(AcrossHibernateJpaModule.NAME)
@EntityScan(basePackageClasses = ActivityLogDomainConfiguration.class)
class EntityScanConfiguration
{
}
