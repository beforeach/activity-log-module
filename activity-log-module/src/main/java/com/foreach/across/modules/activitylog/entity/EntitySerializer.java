package com.foreach.across.modules.activitylog.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyRegistry;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.PersistentProperty;
import org.springframework.stereotype.Component;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class EntitySerializer
{
	private final Logger logger = LoggerFactory.getLogger( getClass() );

	private final EntityRegistry entityRegistry;
	private final ObjectMapper objectMapper;

	@SuppressWarnings("unchecked")
	public String toJson( Object entity ) {
		Object result = serializeEntity( entity );
		try {
			return objectMapper.writeValueAsString( result );
		}
		catch ( JsonProcessingException e ) {
			logger.error( "could not process json: ", e );
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private Object serializeEntity( Object entity ) {
		Class<?> entityType = entity.getClass();

		EntityPropertyRegistry registry = entityRegistry.getEntityConfiguration( entityType ).getPropertyRegistry();
		Collection<EntityPropertyDescriptor> properties = registry.getRegisteredDescriptors();
		return properties.stream()
//		                 .filter( property -> property.hasAttribute( EntityAttributes.NATIVE_PROPERTY_DESCRIPTOR ) )
                         .filter( property -> property.hasAttribute( PersistentProperty.class ) )
                         // we cannot use Collectors.toMap, because it uses Map.merge which does not support null values
                         .collect( LinkedHashMap::new,
                                   ( map, propertyDescriptor ) -> map
		                                   .put( propertyDescriptor.getName(), serializeProperty( propertyDescriptor.getValueFetcher().getValue( entity ) ) ),
                                   LinkedHashMap::putAll );
	}

	@SuppressWarnings("unchecked")
	private Object serializeProperty( Object propertyValue ) {
		if ( propertyValue == null ) {
			return null;
		}

		if ( propertyValue.getClass().isArray() ) {
			return Stream.of( propertyValue ).map( this::serializeProperty );
		}

		if ( Collection.class.isAssignableFrom( propertyValue.getClass() ) ) {
			return ( (Collection) propertyValue ).stream().map( this::serializeProperty );
		}

		if ( ClassUtils.isPrimitiveOrWrapper( propertyValue.getClass() ) ) {
			return propertyValue;
		}

		if ( entityRegistry.contains( propertyValue.getClass() ) ) {
			// handle id reference
			EntityConfiguration entityConfiguration = entityRegistry.getEntityConfiguration( propertyValue.getClass() );
			if ( entityConfiguration.hasAttribute( PersistentEntity.class ) && entityConfiguration.hasEntityModel() ) {
				return entityConfiguration.getId( propertyValue );
			}
			// it's not a repository entity, serialize the whole object
			return serializeEntity( propertyValue );
		}

		if ( Temporal.class.isAssignableFrom( propertyValue.getClass() ) ) {
			return convertTemporal( (Temporal) propertyValue );
		}

		if ( Date.class.isAssignableFrom( propertyValue.getClass() ) ) {
			return convertTemporal( ( (Date) propertyValue ).toInstant() );
		}

		return propertyValue;
	}

	private Map<EntityPropertyDescriptor, Object> fromMap( Map<String, Object> map,
	                                                       Class<?> entityClass ) {
		Map<EntityPropertyDescriptor, Object> result = new LinkedHashMap<>();
		EntityPropertyRegistry registry = entityRegistry.getEntityConfiguration( entityClass ).getPropertyRegistry();
		for ( Map.Entry<String, Object> entry : map.entrySet() ) {
			String key = entry.getKey();
			Object value = entry.getValue();
			EntityPropertyDescriptor property = registry.getProperty( key );
			if ( property != null ) {
				result.put( property, value );
			}
		}
		return result;
	}

	/**
	 * Converts a given {@link Temporal} value by using a corresponding ISO-like DateTimeFormatter.
	 */
	private String convertTemporal( Temporal propertyValue ) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
		if ( propertyValue instanceof ZonedDateTime ) {
			formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
		}
		if ( propertyValue instanceof OffsetDateTime ) {
			formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		}
		if ( propertyValue instanceof OffsetTime ) {
			formatter = DateTimeFormatter.ISO_OFFSET_TIME;
		}
		if ( propertyValue instanceof LocalDateTime ) {
			formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		}
		if ( propertyValue instanceof LocalDate ) {
			formatter = DateTimeFormatter.ISO_LOCAL_DATE;
		}
		if ( propertyValue instanceof LocalTime ) {
			formatter = DateTimeFormatter.ISO_LOCAL_TIME;
		}
		return formatter.format( propertyValue );
	}

}
