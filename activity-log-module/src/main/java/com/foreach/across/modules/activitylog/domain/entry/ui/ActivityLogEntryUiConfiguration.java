package com.foreach.across.modules.activitylog.domain.entry.ui;

import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntry;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.config.builders.EntityListViewFactoryBuilder;
import com.foreach.across.modules.entity.config.builders.EntityPropertyRegistryBuilder;
import com.foreach.across.modules.entity.config.builders.EntityViewFactoryBuilder;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.entity.support.EntityMessageCodeResolver;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.spring.security.infrastructure.services.SecurityPrincipalLabelResolverStrategy;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.foreach.across.modules.web.ui.elements.TextViewElement.text;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Configuration
@RequiredArgsConstructor
class ActivityLogEntryUiConfiguration implements EntityConfigurer
{
	private final ActivityLogEntryAllowableActionsBuilder activityLogEntryAllowableActionsBuilder;
	private final EntityRegistry entityRegistry;
	private final MessageSource messageSource;
	private final SecurityPrincipalLabelResolverStrategy securityPrincipalLabelResolverStrategy;
	private final ConversionService conversionService;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.withType( ActivityLogEntry.class )
		        .allowableActionsBuilder( activityLogEntryAllowableActionsBuilder )
		        .properties(
				        props -> props.and( this::configureLabel )
				                      .and( this::configureActionProperty )
				                      .and( this::configureTypeProperty )
				                      .and( this::configureCreatedProperty )
		        )
		        .detailView( this::configureDetailView )
		        .listView( this::configureListView );
	}

	private void configureListView( EntityListViewFactoryBuilder lvb ) {
		lvb.showProperties( "type", "objectId", "action", "version", "createdDate", "createdBy" )
		   .entityQueryFilter( eqf -> eqf.showProperties( "objectType", "objectId", "action", "createdBy" ) );
	}

	private void configureLabel( EntityPropertyRegistryBuilder props ) {
		props.label()
		     .controller(
				     ctrl -> ctrl.withEntity( ActivityLogEntry.class, String.class )
				                 .valueFetcher( activityLogEntry -> StringUtils
						                 .join( getReadableObjectType( activityLogEntry ), " ", activityLogEntry.getObjectId() ) )
		     );
	}

	private void configureCreatedProperty( EntityPropertyRegistryBuilder props ) {
		props.property( "createdDate" )
		     .hidden( true )
		     .and()
		     .property( "createdBy" )
		     .hidden( true )
		     .and()
		     .property( "created" )
		     .propertyType( String.class )
		     .readable( true )
		     .hidden( true )
		     .controller(
				     ctrl -> ctrl.withTarget( ActivityLogEntry.class, String.class )
				                 .valueFetcher( activityLogEntry -> {
					                 String principal = activityLogEntry.getCreatedBy();
					                 String createdByLabel = StringUtils.defaultString(
							                 securityPrincipalLabelResolverStrategy.resolvePrincipalLabel( principal ), activityLogEntry.getCreatedBy() );
					                 LocalDateTime createdDate = activityLogEntry.getCreatedDate();
					                 /*
					                    Convert to date so that it can be reused within message codes, which do not support the java.time api
					                    due to the use of MessageFormat.
					                  */
					                 Date createdDateAsDate = Date.from( createdDate.toInstant( ZoneId.systemDefault().getRules().getOffset( createdDate ) ) );

					                 if ( messageSource != null ) {
						                 String message = messageSource.getMessage( "ActivityLogEntry.created",
						                                                            new Object[] { createdDateAsDate, createdByLabel },
						                                                            "", LocaleContextHolder.getLocale() );

						                 if ( StringUtils.isNotEmpty( message ) ) {
							                 return message;
						                 }
					                 }

					                 return conversionService.convert( createdDateAsDate, String.class )
							                 + ( !StringUtils.isBlank( principal ) ? " by " + principal : "" );
				                 } )
		     );
	}

	private void configureActionProperty( EntityPropertyRegistryBuilder props ) {
		props.property( "action" )
		     .viewElementBuilder( ViewElementMode.VALUE, this::actionViewElementBuilder )
		     .viewElementBuilder( ViewElementMode.LIST_VALUE, this::actionViewElementBuilder )
		;
	}

	private ViewElement actionViewElementBuilder( ViewElementBuilderContext bc ) {
		String propertyValue = EntityViewElementUtils.currentPropertyValue( bc, String.class );

		if ( bc.hasAttribute( EntityMessageCodeResolver.class ) ) {
			EntityMessageCodeResolver messageCodeResolver = bc.getAttribute( EntityMessageCodeResolver.class );
			propertyValue = messageCodeResolver.getMessage( propertyValue, propertyValue );
		}

		return text( propertyValue );
	}

	private void configureTypeProperty( EntityPropertyRegistryBuilder props ) {
		props.property( "objectType" )
		     .hidden( true )
		     .and()
		     .property( "type" )
		     .propertyType( TypeDescriptor.valueOf( String.class ) )
		     .controller(
				     ctrl -> ctrl.withTarget( ActivityLogEntry.class, String.class )
				                 .valueFetcher( this::getReadableObjectType )
		     );
	}

	private String getReadableObjectType( ActivityLogEntry activityLogEntry ) {
		Class<?> clazz = null;
		try {
			clazz = ClassUtils.getClass( activityLogEntry.getObjectType() );
		}
		catch ( ClassNotFoundException ignore ) {

		}

		if ( clazz != null && entityRegistry.contains( clazz ) ) {
			return entityRegistry.getEntityConfiguration( clazz ).getDisplayName();
		}
		return activityLogEntry.getObjectType();
	}

	private void configureDetailView( EntityViewFactoryBuilder vb ) {
		vb.properties(
				props -> props.property( "created" )
				              .hidden( false )
		).viewProcessor( vp -> vp.createBean( ActivityLogEntryLayoutViewProcessor.class ) )
		  .viewProcessor( vp -> vp.createBean( ActivityLogPropertyRegistryViewProcessor.class ) )
		  .viewProcessor( vp -> vp.createBean( ActivityLogPropertyRenderingViewProcessor.class ).order( 1100 ) );
	}
}
