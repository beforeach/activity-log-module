package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;

/**
 * Registrar that supports creating properties based on a given {@link Object}.
 *
 * @see PropertyValueBasedRegistryProvider
 */
public interface PropertyValueBasedRegistrar
{
	/**
	 * Checks whether this {@link PropertyValueBasedRegistrar} can create properties based on the given value.
	 *
	 * @param value for which one or more property descriptors should be created.
	 * @return {@code true} if this registrar can handle the given property type.
	 */
	boolean handles( Object value );

	/**
	 * Creates a {@link MutableEntityPropertyDescriptor} for a given {@link Object}
	 *
	 * @param parentDescriptor parent descriptor for the property, for which for example the value can be retrieved.
	 * @param propertyName     of the new property that should be created
	 * @param value            that defines what the property looks like
	 * @param propertyRegistry the property registry of the parent descriptor, in which this property descriptor will also be registered.
	 * @return a new {@link MutableEntityPropertyDescriptor} for the given property
	 */
	MutableEntityPropertyDescriptor createPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                                          String propertyName,
	                                                          Object value,
	                                                          MutableEntityPropertyRegistry propertyRegistry );

	/**
	 * Supports creating additional property descriptors after the creation of the descriptor for the given value.
	 * Can be used for example to create nested property descriptors, or additional indexer / map descriptors.
	 *
	 * @param valueDescriptor  that has just been created based on the given value
	 * @param value            for which additional descriptors could be registered
	 * @param propertyRegistry registry in which the value descriptor has been registered.
	 */
	void registerMemberDescriptors( MutableEntityPropertyDescriptor valueDescriptor, Object value, MutableEntityPropertyRegistry propertyRegistry );
}
