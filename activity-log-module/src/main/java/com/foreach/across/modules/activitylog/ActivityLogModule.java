package com.foreach.across.modules.activitylog;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.bootstrap.AcrossBootstrapConfig;
import com.foreach.across.core.context.bootstrap.AcrossBootstrapConfigurer;
import com.foreach.across.core.context.bootstrap.ModuleBootstrapConfig;
import com.foreach.across.core.context.configurer.ApplicationContextConfigurer;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;

import java.util.Set;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@AcrossDepends(required = { AcrossHibernateJpaModule.NAME, EntityModule.NAME })
public class ActivityLogModule extends AcrossModule
{
	public static final String NAME = "ActivityLogModule";
	public static final String RESOURCES = "activitylog";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return "Supports persisting and querying auditing information for entities within an application";
	}

	@Override
	public String getResourcesKey() {
		return RESOURCES;
	}

	@Override
	protected void registerDefaultApplicationContextConfigurers( Set<ApplicationContextConfigurer> contextConfigurers ) {
	}

	@Override
	public void prepareForBootstrap( ModuleBootstrapConfig currentModule, AcrossBootstrapConfig contextConfig ) {
		// current module simply extends EntityModule
		contextConfig.extendModule( AcrossBootstrapConfigurer.CONTEXT_POSTPROCESSOR_MODULE, ComponentScanConfigurer.forAcrossModule( getClass() ) );
	}
}
