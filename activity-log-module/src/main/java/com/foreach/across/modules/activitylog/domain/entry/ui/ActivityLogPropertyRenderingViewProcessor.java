package com.foreach.across.modules.activitylog.domain.entry.ui;

import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntry;
import com.foreach.across.modules.activitylog.domain.entry.DefaultActivityLogAction;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertySelector;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.WebDataBinder;

import java.util.List;
import java.util.Map;

import static com.foreach.across.modules.entity.views.processors.PropertyRenderingViewProcessor.ATTRIBUTE_PROPERTY_DESCRIPTORS;

public class ActivityLogPropertyRenderingViewProcessor extends EntityViewProcessorAdapter
{
	@Override
	public void initializeCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		EntityViewContext entityViewContext = entityViewRequest.getEntityViewContext();
		if ( entityViewContext.holdsEntity() && StringUtils.startsWith( entityViewContext.getEntity( ActivityLogEntry.class ).getAction(),
		                                                                DefaultActivityLogAction.PREFIX ) ) {
			EntityPropertySelector selector = EntityPropertySelector.of( "content" );
			List<EntityPropertyDescriptor> properties = entityViewRequest.getEntityViewContext().getPropertyRegistry().select( selector );
			Map<String, EntityPropertyDescriptor> descriptorMap =
					(Map<String, EntityPropertyDescriptor>) entityViewRequest.getModel().get( ATTRIBUTE_PROPERTY_DESCRIPTORS );
			properties.forEach( p -> descriptorMap.put( p.getName(), p ) );
			descriptorMap.remove( "body" );
			entityViewRequest.getModel().put( ATTRIBUTE_PROPERTY_DESCRIPTORS, descriptorMap );
		}
	}
}
