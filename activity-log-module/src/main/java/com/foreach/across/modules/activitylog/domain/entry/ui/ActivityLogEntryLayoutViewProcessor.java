package com.foreach.across.modules.activitylog.domain.entry.ui;

import com.foreach.across.modules.bootstrapui.elements.ColumnViewElement;
import com.foreach.across.modules.bootstrapui.styles.AcrossBootstrapStyles;
import com.foreach.across.modules.bootstrapui.styles.BootstrapStyles;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SingleEntityFormViewProcessor;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.web.ui.ViewElement;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.modules.web.ui.elements.NodeViewElement;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;

import java.util.stream.Stream;

public class ActivityLogEntryLayoutViewProcessor extends EntityViewProcessorAdapter
{
	@Override
	protected void postRender( EntityViewRequest entityViewRequest,
	                           EntityView entityView,
	                           ContainerViewElement container,
	                           ViewElementBuilderContext builderContext ) {
		NodeViewElement typeInfo = wrapElements( "typeInfo", container.removeAllFromTree( "formGroup-type", "formGroup-objectId" ) );
		container.addChild( typeInfo );

		NodeViewElement versionInfo = wrapElements( "versionInfo", container.removeAllFromTree( "formGroup-action", "formGroup-version" ) );
		container.addChild( versionInfo );

		ContainerViewElementUtils.find( container, SingleEntityFormViewProcessor.RIGHT_COLUMN, ColumnViewElement.class )
		                         .ifPresent(
				                         cve -> ContainerViewElementUtils.removeAll( container, "typeInfo", "versionInfo", "formGroup-created" )
				                                                         .forEach( cve::addChild )
		                         );
	}

	private NodeViewElement wrapElements( String wrapperElementName, Stream<ViewElement> childrenToAdd ) {
		NodeViewElement wrapper = new NodeViewElement( wrapperElementName, "div" )
				.set( AcrossBootstrapStyles.css.display.flex, AcrossBootstrapStyles.css.flex.row );
		childrenToAdd.forEach( ve -> {
			ve.set( AcrossBootstrapStyles.css.padding.horizontal.none, BootstrapStyles.css.grid.column6 );
			wrapper.addChild( ve );
		} );

		return wrapper;
	}

}
