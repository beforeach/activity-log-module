package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.core.annotations.RefreshableCollection;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

/**
 * Supports creating property descriptors for a given {@link Object}.
 */
@Service
public class PropertyValueBasedRegistryProvider
{
	private Collection<PropertyValueBasedRegistrar> valueBasedRegistrars = Collections.emptyList();

	@RefreshableCollection(includeModuleInternals = true, incremental = true)
	void setPostProcessors( Collection<PropertyValueBasedRegistrar> valueBasedRegistrars ) {
		this.valueBasedRegistrars = valueBasedRegistrars;
	}

	/**
	 * Registers one or more property descriptors in the given {@link MutableEntityPropertyRegistry} based on a given {@link Object}.
	 *
	 * @param parentDescriptor for the properties that are created
	 * @param propertyName     of the main descriptor that should be created
	 * @param value            that defines the type of the descriptor and whether additional descriptors are made
	 * @param propertyRegistry in which the properties should be registered
	 */
	public void registerPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                        String propertyName,
	                                        Object value,
	                                        MutableEntityPropertyRegistry propertyRegistry ) {
		valueBasedRegistrars.stream()
		                    .filter( pvbr -> pvbr.handles( value ) )
		                    .findFirst()
		                    .ifPresent( pvbr -> {
			                    MutableEntityPropertyDescriptor descriptor = pvbr.createPropertyDescriptor( parentDescriptor, propertyName, value,
			                                                                                                propertyRegistry );
			                    propertyRegistry.register( descriptor );
			                    pvbr.registerMemberDescriptors( descriptor, value, propertyRegistry );
		                    } );
	}
}
