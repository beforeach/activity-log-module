package com.foreach.across.modules.activitylog.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.installers.AcrossLiquibaseInstaller;
import org.springframework.core.annotation.Order;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Order(1)
@Installer(description = "Installs the required database tables", version = 1)
public class ActivityLogSchemaInstaller extends AcrossLiquibaseInstaller
{
	public ActivityLogSchemaInstaller() {
		super( "installers/ActivityLogModule/schema.xml" );
	}
}
