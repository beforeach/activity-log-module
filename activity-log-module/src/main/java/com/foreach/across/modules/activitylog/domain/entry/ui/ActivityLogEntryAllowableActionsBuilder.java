package com.foreach.across.modules.activitylog.domain.entry.ui;

import com.foreach.across.modules.entity.actions.EntityConfigurationAllowableActionsBuilder;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.spring.security.actions.AllowableAction;
import com.foreach.across.modules.spring.security.actions.AllowableActionSet;
import com.foreach.across.modules.spring.security.actions.AllowableActions;
import com.foreach.across.modules.spring.security.infrastructure.services.CurrentSecurityPrincipalProxy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ActivityLogEntryAllowableActionsBuilder implements EntityConfigurationAllowableActionsBuilder
{
	private final CurrentSecurityPrincipalProxy currentSecurityPrincipalProxy;

	@Override
	public AllowableActions getAllowableActions( EntityConfiguration<?> entityConfiguration ) {
		return getAllowableActions();
	}

	@Override
	public <V> AllowableActions getAllowableActions( EntityConfiguration<V> entityConfiguration, V entity ) {
		return getAllowableActions();
	}

	private AllowableActions getAllowableActions() {
		if ( currentSecurityPrincipalProxy.hasAuthority( "access administration" ) ) {
			return new AllowableActionSet( AllowableAction.READ );
		}
		return new AllowableActionSet();
	}
}
