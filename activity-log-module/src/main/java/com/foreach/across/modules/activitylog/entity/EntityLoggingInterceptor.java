package com.foreach.across.modules.activitylog.entity;

import com.foreach.across.modules.activitylog.config.ActivityLogModuleSettings;
import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntry;
import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntryRepository;
import com.foreach.across.modules.activitylog.domain.entry.DefaultActivityLogAction;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.hibernate.aop.EntityInterceptorAdapter;
import com.foreach.across.modules.spring.security.infrastructure.business.SecurityPrincipal;
import com.foreach.across.modules.spring.security.infrastructure.business.SecurityPrincipalId;
import com.foreach.across.modules.spring.security.infrastructure.services.CurrentSecurityPrincipalProxy;
import com.foreach.across.modules.spring.security.infrastructure.services.SecurityPrincipalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * {@link com.foreach.across.modules.hibernate.aop.EntityInterceptor} that is responsible for creating an {@link ActivityLogEntry}
 * whenever an entity is created, updated or deleted.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class EntityLoggingInterceptor extends EntityInterceptorAdapter<Object>
{
	private final EntityRegistry entityRegistry;
	private final ActivityLogEntryRepository entityHistoryVersionRepository;
	private final EntitySerializer entitySerializer;
	private final SecurityPrincipalService securityPrincipalService;
	private final ActivityLogModuleSettings moduleSettings;
	private final PlatformTransactionManager transactionManager;
	private final CurrentSecurityPrincipalProxy currentPrincipal;

	@Override
	public boolean handles( Class<?> entityClass ) {
		return !ActivityLogEntry.class.isAssignableFrom( entityClass ) && entityRegistry.contains( entityClass );
	}

	@Override
	public void afterCreate( Object entity ) {
		createNewVersion( entity, DefaultActivityLogAction.CREATE );
	}

	@Override
	public void afterUpdate( Object entity ) {
		createNewVersion( entity, DefaultActivityLogAction.UPDATE );
	}

	@Override
	public void afterDelete( Object entity ) {
		createNewVersion( entity, DefaultActivityLogAction.DELETE );
	}

	private void createNewVersion( Object entity, String action ) {
		TransactionStatus transaction =
				moduleSettings.isUseSeparateTransaction()
						? transactionManager.getTransaction( new DefaultTransactionDefinition( TransactionDefinition.PROPAGATION_REQUIRES_NEW ) )
						: transactionManager.getTransaction( new DefaultTransactionDefinition( TransactionDefinition.PROPAGATION_REQUIRED ) );
		try {

			EntityConfiguration entityConfiguration = entityRegistry.getEntityConfiguration( entity );

			String objectType = entity.getClass().getCanonicalName();
			Serializable objectId = entityConfiguration.getId( entity );
			Integer versionNumber = resolveVersion( objectType, objectId, action );

			ActivityLogEntry version = ActivityLogEntry.builder()
			                                           .objectType( objectType )
			                                           .objectId( objectId.toString() )
			                                           .version( versionNumber )
			                                           .action( action )
			                                           .body( entitySerializer.toJson( entity ) )
			                                           .createdDate( LocalDateTime.now() )
			                                           .createdBy( getSecurityPrincipalOrDefaultToSystem().toString() )
			                                           .build();
			entityHistoryVersionRepository.save( version );

			transactionManager.commit( transaction );
		}
		catch ( Exception e ) {
			if ( moduleSettings.isUseSeparateTransaction() ) {
				LOG.error( "Unexpected error occurred whilst saving an activity log", e );
				if ( !transaction.isCompleted() ) {
					transactionManager.rollback( transaction );
				}
			}
			else {
				throw e;
			}
		}
	}

	private Integer resolveVersion( String objectType, Serializable objectId, String action ) {
		Integer versionNumber = 1;

		if ( StringUtils.equals( action, DefaultActivityLogAction.UPDATE ) ) {
			Optional<ActivityLogEntry> latestVersion =
					entityHistoryVersionRepository.findFirstByObjectTypeAndObjectIdOrderByVersionDesc( objectType, objectId.toString() );
			if ( latestVersion.isPresent() ) {
				Integer versionNumberFromDb = latestVersion.get().getVersion();
				versionNumber += versionNumberFromDb;
			}
		}
		return versionNumber;
	}

	private SecurityPrincipalId getSecurityPrincipalOrDefaultToSystem() {
		if ( currentPrincipal.isAuthenticated() ) {
			return currentPrincipal.getSecurityPrincipalId();
		}
		return securityPrincipalService.getPrincipalByName( "system" )
		                               .map( SecurityPrincipal::getSecurityPrincipalId )
		                               .orElseGet( () -> SecurityPrincipalId.of( "system" ) );
	}
}
