package com.foreach.across.modules.activitylog.domain.entry.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntry;
import com.foreach.across.modules.activitylog.domain.entry.DefaultActivityLogAction;
import com.foreach.across.modules.activitylog.domain.entry.ui.property.PropertyValueBasedRegistryProvider;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.entity.EntityAttributes;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.*;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.context.ConfigurableEntityViewContext;
import com.foreach.across.modules.entity.views.context.EntityViewContext;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.request.EntityViewCommand;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.web.bind.WebDataBinder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class ActivityLogPropertyRegistryViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityPropertyRegistryProvider entityPropertyRegistryProvider;
	private final EntityPropertyDescriptorFactory entityPropertyDescriptorFactory;
	private final PropertyValueBasedRegistryProvider propertyValueBasedRegistryProvider;

	@Override
	@SuppressWarnings("unchecked")
	public void initializeCommandObject( EntityViewRequest entityViewRequest, EntityViewCommand command, WebDataBinder dataBinder ) {
		EntityViewContext entityViewContext = entityViewRequest.getEntityViewContext();
		if ( ConfigurableEntityViewContext.class.isAssignableFrom( entityViewContext.getClass() ) ) {
			ActivityLogEntry entity = entityViewContext.getEntity( ActivityLogEntry.class );
			if ( StringUtils.startsWith( entity.getAction(), DefaultActivityLogAction.PREFIX ) ) {
				EntityPropertyRegistry customizedPropertyRegistry =
						createForOriginalRegistry( entity, entityViewContext.getPropertyRegistry() );
				( (ConfigurableEntityViewContext) entityViewContext ).setPropertyRegistry( customizedPropertyRegistry );
			}
		}
	}

	public EntityPropertyRegistry createForOriginalRegistry( ActivityLogEntry entity, EntityPropertyRegistry propertyRegistry ) {
		MergingEntityPropertyRegistry customizedPropertyRegistry = new MergingEntityPropertyRegistry( propertyRegistry, entityPropertyRegistryProvider,
		                                                                                              entityPropertyDescriptorFactory );
		Map<String, Object> dataAsMap = getObjectAsMap( entity );

		MutableEntityPropertyDescriptor content = new EntityPropertyDescriptorBuilder( "content" )
				.propertyType( Object.class )
				.propertyType( TypeDescriptor.valueOf( Object.class ) )
				.controller( ctrl -> ctrl.valueFetcher( o -> dataAsMap ) )
				.viewElementType( ViewElementMode.FORM_READ, BootstrapUiElements.FIELDSET )
				.attribute( EntityAttributes.IS_EMBEDDED_OBJECT, true )
				.attribute( EntityAttributes.FIELDSET_PROPERTY_SELECTOR, EntityPropertySelector.all() )
				.readable( true )
				.writable( false )
				.hidden( false )
				.build();

		customizedPropertyRegistry.register( content );

		// todo workaround: create custom PropertyRegistries for fieldset entities because the EntityPropertySelector filters out nested values.
		DefaultEntityPropertyRegistry contentPropertyRegistry = new DefaultEntityPropertyRegistry( entityPropertyRegistryProvider );
		dataAsMap.forEach( ( propertyName, propertyValue ) -> propertyValueBasedRegistryProvider
				.registerPropertyDescriptor( content, propertyName, propertyValue, contentPropertyRegistry ) );
		content.setPropertyRegistry( contentPropertyRegistry );

		return customizedPropertyRegistry;
	}

	/**
	 * Retrieves the body of an {@link ActivityLogEntry} and it converts it to a {@link Map}.
	 * If the content can not be converted, an empty map is returned.
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> getObjectAsMap( ActivityLogEntry entity ) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> dataAsMap = new HashMap<>();
		try {
			dataAsMap = new HashMap<>( objectMapper.readValue( entity.getBody(), Map.class ) );
		}
		catch ( IOException e ) {
			LOG.error( "Unable to parse entry content", e );
		}
		return dataAsMap;
	}
}
