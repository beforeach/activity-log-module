package com.foreach.across.modules.activitylog.domain.entry;

import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "alm_activity_log_entry")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityLogEntry extends SettableIdBasedEntity<ActivityLogEntry>
{
	@Id
	@GeneratedValue(generator = "seq_alm_entry_id")
	@GenericGenerator(
			name = "seq_alm_entry_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_alm_entry_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@Length(max = 255)
	@Column(name = "object_type")
	private String objectType;

	@Column(name = "body")
	private String body;

	@Column(name = "action")
	@Length(max = 50) // todo leave a length validation? or remove it so an application can modify table column lengths?
	private String action;

	@Min(1)
	@Column(name = "version")
	private Integer version;

	@Column(name = "object_id")
	@Length(max = 100) // todo see above
	private String objectId;

	@NotNull
	@Column(name = "created_date", nullable = false)
	private LocalDateTime createdDate;

	@Column(name = "created_by")
	private String createdBy;
}
