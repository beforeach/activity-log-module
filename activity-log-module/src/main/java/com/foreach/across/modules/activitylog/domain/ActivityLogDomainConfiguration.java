package com.foreach.across.modules.activitylog.domain;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import org.springframework.context.annotation.Configuration;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Configuration
@EnableAcrossJpaRepositories
public class ActivityLogDomainConfiguration
{
}
