package com.foreach.across.modules.activitylog.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ConfigurationProperties(value = "activity-log-module")
@Component
public class ActivityLogModuleSettings
{
	/**
	 * Configures whether {@link com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntry}s should be created in a separate transaction.
	 * If {@code true}, a failure in creating the activity log entry will not influence the actual updating (CUD action) of an entity.
	 * If {@code false} and an error occurs during the creation of the entry, then the update of the entity in question will be rollbacked as well.
	 */
	private boolean useSeparateTransaction = false;
}
