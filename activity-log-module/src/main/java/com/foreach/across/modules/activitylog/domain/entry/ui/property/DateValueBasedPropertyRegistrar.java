package com.foreach.across.modules.activitylog.domain.entry.ui.property;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.entity.config.builders.EntityPropertyDescriptorBuilder;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyRegistry;
import com.foreach.across.modules.entity.views.ViewElementMode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.time.*;
import java.time.temporal.Temporal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * {@link PropertyValueBasedRegistrar} that supports creating property descriptors for {@link Temporal} types.
 */
@Component
@RequiredArgsConstructor
@OrderInModule(30000)
public class DateValueBasedPropertyRegistrar extends BasePropertyValueBasedRegistrar
{
	@Override
	public boolean handles( Object value ) {
		return value != null && checkForDateType( value.toString() ).isPresent();
	}

	@Override
	public MutableEntityPropertyDescriptor createPropertyDescriptor( MutableEntityPropertyDescriptor parentDescriptor,
	                                                                 String propertyName,
	                                                                 Object value,
	                                                                 MutableEntityPropertyRegistry propertyRegistry ) {
		TemporalHolder holder = checkForDateType( value.toString() ).get();

		EntityPropertyDescriptorBuilder descriptorBuilder =
				createBasePropertyDescriptorBuilder( parentDescriptor, propertyName )
						.propertyType( holder.getTemporal().getClass() )
						.propertyType( TypeDescriptor.valueOf( holder.getTemporal().getClass() ) )
						.viewElementType( ViewElementMode.VALUE, BootstrapUiElements.DATETIME )
						.controller(
								controller -> controller.valueFetcher( mapPropertyValueFetcher( parentDescriptor, propertyName, holder.getTemporal() )
										                                       .andThen( fetchedValue -> {
											                                       if ( !( fetchedValue instanceof Temporal ) ) {
												                                       return holder.getFormatter().apply( fetchedValue.toString() );
											                                       }
											                                       return fetchedValue;
										                                       } ) )
						);

		return descriptorBuilder.build();
	}

	private Optional<TemporalHolder> checkForDateType( String propertyValue ) {
		// ZonedDateTime handles both OffsetDateTime and Instant as well
		List<Function<String, Temporal>> formatters = Arrays.asList( ZonedDateTime::parse, OffsetTime::parse, LocalDateTime::parse, LocalDate::parse,
		                                                             LocalTime::parse );
		return formatters.stream()
		                 .map( formatter -> {
			                 try {
				                 return new TemporalHolder( formatter.apply( propertyValue ), formatter );
			                 }
			                 catch ( Exception ignore ) {

			                 }
			                 return null;
		                 } )
		                 .filter( Objects::nonNull )
		                 .findFirst();
	}

	@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
	@Getter
	static class TemporalHolder
	{
		private final Temporal temporal;
		private final Function<String, Temporal> formatter;
	}
}
