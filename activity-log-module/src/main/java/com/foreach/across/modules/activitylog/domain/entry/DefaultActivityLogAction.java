package com.foreach.across.modules.activitylog.domain.entry;

/**
 * Default {@link ActivityLogEntry#getAction()} actions that are used by this module.
 * These are actions that are logged automatically when the module is active.
 *
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface DefaultActivityLogAction
{
	String PREFIX = "ALM_";

	String CREATE = "ALM_CREATE";
	String UPDATE = "ALM_UPDATE";
	String DELETE = "ALM_DELETE";
}
