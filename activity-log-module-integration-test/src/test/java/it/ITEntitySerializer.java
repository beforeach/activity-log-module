package it;

import com.foreach.across.modules.activitylog.ActivityLogModule;
import com.foreach.across.modules.activitylog.entity.EntitySerializer;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.test.ExposeForTest;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ITEntitySerializer.Config.class })
@ExposeForTest(EntitySerializer.class)
public class ITEntitySerializer
{
	@Autowired
	private EntitySerializer entitySerializer;

	void serializeSimpleEntity() {

	}

	void serializeEntityWithRelationships() {

	}

	void serializeEntityWithEmbeddedFields() {

	}

	void serializeEntityWithEmbeddedFieldsHoldingRelationships() {

	}

	@Configuration
	static class Config
	{
		@Bean
		public ActivityLogModule activityLogModule() {
			return new ActivityLogModule();
		}

		@Bean
		public EntityModule entityModule() {
			return new EntityModule();
		}

		@Bean
		public AcrossHibernateJpaModule acrossHibernateJpaModule() {
			return new AcrossHibernateJpaModule();
		}
	}
}
