package it.support;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.activitylog.entity.EntitySerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import static org.mockito.Mockito.mock;

@Configuration
public class MockSerializerConfig
{
	@Bean
	@Primary
	@Exposed
	public EntitySerializer entitySerializer() {
		return mock( EntitySerializer.class );
	}
}