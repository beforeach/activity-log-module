package it;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.core.context.bootstrap.AcrossBootstrapConfig;
import com.foreach.across.core.context.bootstrap.AcrossBootstrapConfigurer;
import com.foreach.across.modules.activitylog.ActivityLogModule;
import com.foreach.across.modules.activitylog.config.ActivityLogModuleSettings;
import com.foreach.across.modules.activitylog.domain.entry.ActivityLogEntryRepository;
import com.foreach.across.modules.activitylog.entity.EntitySerializer;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.spring.security.SpringSecurityModule;
import com.foreach.across.modules.spring.security.configuration.AcrossWebSecurityConfigurer;
import com.foreach.across.modules.web.AcrossWebModule;
import com.foreach.across.test.AcrossTestConfiguration;
import com.foreach.across.test.AcrossWebAppConfiguration;
import com.foreach.across.test.ExposeForTest;
import com.foreach.across.test.support.config.TestDataSourceConfigurer;
import com.foreach.across.testmodules.springdata.SpringDataJpaModule;
import com.foreach.across.testmodules.springdata.domain.user.User;
import com.foreach.across.testmodules.springdata.domain.user.UserRepository;
import it.support.MockSerializerConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@AcrossWebAppConfiguration
@TestPropertySource(
		properties = {
				"spring.datasource.url=jdbc:hsqldb:mem:single-db",
				"spring.jpa.show-sql=true",
				"spring.transaction.default-timeout=25s",
				"acrossHibernate.hibernateProperties[hibernate.hbm2ddl.auto]=create-drop"
		})
public class ITEntityLoggingInterceptorTransactions
{
	@Autowired
	private ActivityLogModuleSettings activityLogModuleSettings;

	@Autowired
	private ActivityLogEntryRepository activityLogEntryRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EntitySerializer entitySerializer;

	@BeforeEach
	void setUp() {
		activityLogEntryRepository.deleteAll();
		userRepository.deleteAll();
		reset( entitySerializer );
	}

	@Test
	void activityLogErrorsDoNotInfluenceUpdatingAnEntity() {
		activityLogModuleSettings.setUseSeparateTransaction( true );
		assertThat( userRepository.findAll() ).isEmpty();
		assertThat( activityLogEntryRepository.findAll() ).isEmpty();
		User user = User.builder()
		                .firstName( "john" )
		                .lastName( "doe" )
		                .build();
		when( entitySerializer.toJson( any( User.class ) ) ).thenThrow( new NullPointerException() );
		userRepository.save( user );
		assertThat( userRepository.findAll() ).isNotEmpty();
		assertThat( activityLogEntryRepository.findAll() ).isEmpty();
	}

	@Test
	void activityLogErrorCauseTheEntityNotToBeUpdated() {
		activityLogModuleSettings.setUseSeparateTransaction( false );
		assertThat( userRepository.findAll() ).isEmpty();
		assertThat( activityLogEntryRepository.findAll() ).isEmpty();
		User user = User.builder()
		                .firstName( "john" )
		                .lastName( "doe" )
		                .build();
		when( entitySerializer.toJson( any( User.class ) ) ).thenThrow( new NullPointerException() );
		assertThatThrownBy( () -> userRepository.save( user ) )
				.isInstanceOf( NullPointerException.class );
		assertThat( userRepository.findAll() ).isEmpty();
		assertThat( activityLogEntryRepository.findAll() ).isEmpty();
	}

	@AcrossTestConfiguration(modules = { AcrossHibernateJpaModule.NAME, ActivityLogModule.NAME, EntityModule.NAME, AcrossWebModule.NAME,
	                                     SpringSecurityModule.NAME })
	@Import({ TestDataSourceConfigurer.class, BootstrapConfigurer.class })
	@ExposeForTest({ ActivityLogModuleSettings.class })
	static class Config
	{
		@Bean
		public SpringDataJpaModule springDataJpaModule() {
			return new SpringDataJpaModule();
		}

		@Bean
		public AcrossWebSecurityConfigurer springSecurityWebConfigurer() {
			return new SimpleSpringSecurityConfigurer();
		}
	}

	/**
	 * At least one SpringSecurityConfigurer should be present.
	 */
	@Configuration
	@OrderInModule(2)
	protected static class SimpleSpringSecurityConfigurer implements AcrossWebSecurityConfigurer
	{
		@Override
		public void configure( AuthenticationManagerBuilder auth ) throws Exception {
			auth.inMemoryAuthentication().withUser( "test" ).password( "test" ).roles( "test" );
		}

		@Override
		public void configure( HttpSecurity http ) throws Exception {
			http
					.authorizeRequests()
					.anyRequest().authenticated()
					.and()
					.formLogin().and()
					.httpBasic();
		}
	}

	@Configuration
	protected static class BootstrapConfigurer implements AcrossBootstrapConfigurer
	{
		@Override
		public void configureContext( AcrossBootstrapConfig contextConfiguration ) {
			contextConfiguration.extendModule( AcrossBootstrapConfigurer.CONTEXT_POSTPROCESSOR_MODULE, MockSerializerConfig.class );
		}
	}

}
