package com.foreach.across.testmodules.springdata.domain.serie;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface SerieRepository extends IdBasedEntityJpaRepository<Serie>
{
}
