package com.foreach.across.testmodules.springdata.domain.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface AuthorRepository extends JpaSpecificationExecutor<Author>, JpaRepository<Author, Long>
{
}
