package com.foreach.across.testmodules.springdata.domain.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.UUID;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
public interface BookRepository extends JpaSpecificationExecutor<Book>, JpaRepository<Book, UUID>
{
}
