package com.foreach.across.testmodules.springdata;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.configurer.ApplicationContextConfigurer;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;

import java.util.Set;

@AcrossDepends(required = { AcrossHibernateJpaModule.NAME, EntityModule.NAME })
public class SpringDataJpaModule extends AcrossModule
{
	public static final String NAME = "SpringDataJpaModule";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return "Module containing a single JPA managed entity through a generated Spring data repository.";
	}

	@Override
	protected void registerDefaultApplicationContextConfigurers( Set<ApplicationContextConfigurer> contextConfigurers ) {
		contextConfigurers.add( ComponentScanConfigurer.forAcrossModule( getClass() ) );
	}

}

