package com.foreach.across.testmodules.springdata.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.testmodules.springdata.domain.author.Author;
import com.foreach.across.testmodules.springdata.domain.author.AuthorRepository;
import com.foreach.across.testmodules.springdata.domain.book.Book;
import com.foreach.across.testmodules.springdata.domain.book.BookRepository;
import com.foreach.across.testmodules.springdata.domain.order.Order;
import com.foreach.across.testmodules.springdata.domain.order.OrderRepository;
import com.foreach.across.testmodules.springdata.domain.serie.Serie;
import com.foreach.across.testmodules.springdata.domain.serie.SerieRepository;
import com.foreach.across.testmodules.springdata.domain.user.Address;
import com.foreach.across.testmodules.springdata.domain.user.User;
import com.foreach.across.testmodules.springdata.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;

@Profile("testdata")
@Installer(name = "test-data-installer", description = "Creates initial test data", phase = InstallerPhase.AfterContextBootstrap, version = 1)
@RequiredArgsConstructor
public class TestDataInstaller
{
	private final SerieRepository serieRepository;
	private final AuthorRepository authorRepository;
	private final BookRepository bookRepository;
	private final UserRepository userRepository;
	private final OrderRepository orderRepository;

	@InstallerMethod
	public void configureTestData() {
		orderRepository.deleteAll();
		userRepository.deleteAll();
		bookRepository.deleteAll();
		serieRepository.deleteAll();
		authorRepository.deleteAll();

		Author willyVandersteen = author( "Willy", "Vandersteen", LocalDate.of( 1990, 8, 28 ) );

		Serie redSeries = serieWithParent( "The Red Series", null );
		Serie flemishNonColoured = serieWithParent( "Flemish non-coloured", redSeries );
		Serie blueSeries = serieWithParent( "The Blue Series", null );

		Book chocowakije = book( "Rikki en Wiske in Chocowakije", flemishNonColoured, willyVandersteen );
		Book amoras = book( "Op het eiland Amoras", flemishNonColoured, willyVandersteen );
		Book hoboken = book( "An Island called Hoboken", redSeries, willyVandersteen );
		Book spaanseSpook = book( "Het Spaanse spook", blueSeries, willyVandersteen );
		Book tartaarseHelm = book( "De Tartaarse helm", blueSeries, willyVandersteen );

		User josLemmens = user( "Jos", "Lemmens", LocalDate.of( 2001, 2, 5 ), address( "Vinkeniersweg", 12, 2150, "Belgium" ) );
		User janDeMan = user( "Jan", "De Man", LocalDate.of( 1964, 9, 2 ), address( "Klavertjesstraat", 25, 2548, "Belgium" ) );

		order( josLemmens, LocalDateTime.of( LocalDate.of( 2020, 2, 10 ), LocalTime.of( 17, 54 ) ), chocowakije, amoras, spaanseSpook, tartaarseHelm );
		order( janDeMan, LocalDateTime.of( LocalDate.of( 2015, 5, 19 ), LocalTime.of( 14, 15 ) ), hoboken );

	}

	private Serie serieWithParent( String name, Serie parent ) {
		return serieRepository.save( Serie.builder()
		                                  .name( name )
		                                  .parent( parent )
		                                  .build() );
	}

	private Author author( String firstName, String lastName, LocalDate dateOfBirth ) {
		return authorRepository.save( Author.builder()
		                                    .firstName( firstName )
		                                    .lastName( lastName )
		                                    .dateOfBirth( dateOfBirth )
		                                    .build() );
	}

	private User user( String firstName, String lastName, LocalDate dateOfBirth, Address address ) {
		return userRepository.save( User.builder()
		                                .firstName( firstName )
		                                .lastName( lastName )
		                                .email( StringUtils.join( firstName, ".", lastName, "@localhost" ).toLowerCase().replaceAll( " ", "" ) )
		                                .dateOfBirth( dateOfBirth )
		                                .address( address )
		                                .build() );
	}

	private Book book( String title, Serie serie, Author author ) {
		return bookRepository.save( Book.builder()
		                                .author( author )
		                                .serie( serie )
		                                .title( title )
		                                .build() );
	}

	private Order order( User customer, LocalDateTime orderDate, Book... books ) {
		return orderRepository.save( Order.builder()
		                                  .customer( customer )
		                                  .orderDate( orderDate )
		                                  .books( Arrays.asList( books ) )
		                                  .build() );
	}

	private Address address( String street, Integer streetNumber, Integer zipCode, String country ) {
		return Address.builder()
		              .street( street )
		              .streetNumber( streetNumber )
		              .zipCode( zipCode )
		              .country( country )
		              .build();
	}
}
