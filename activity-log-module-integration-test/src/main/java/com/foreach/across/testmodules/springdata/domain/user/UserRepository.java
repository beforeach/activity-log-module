package com.foreach.across.testmodules.springdata.domain.user;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;

public interface UserRepository extends IdBasedEntityJpaRepository<User>
{
}
