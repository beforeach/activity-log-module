package com.foreach.across.testmodules.springdata.domain.serie;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Table(name = "alm_test_serie")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Serie extends SettableIdAuditableEntity<Serie>
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_alm_test_serie_id")
	@GenericGenerator(
			name = "seq_alm_test_serie_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_alm_test_serie_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@NotBlank
	@Length(max = 250)
	@Column(name = "name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Serie parent;
}