package com.foreach.across.testmodules.springdata.domain.author;

import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Table(name = "alm_test_author")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Author implements Persistable<Long>
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_alm_test_author_id")
	@GenericGenerator(
			name = "seq_alm_test_author_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_alm_test_author_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@Length(max = 50)
	@Column(name = "first_name")
	private String firstName;

	@Length(max = 50)
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "date_of_birth")
	private LocalDate dateOfBirth;

	@Override
	public boolean isNew() {
		return id == null || id == 0;
	}

	public String getName() {
		return StringUtils.join( firstName, " ", lastName );
	}
}
