package com.foreach.across.testmodules.springdata.domain.book;

import com.foreach.across.testmodules.springdata.domain.author.Author;
import com.foreach.across.testmodules.springdata.domain.serie.Serie;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Table(name = "alm_test_book")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Book implements Persistable<UUID>
{
	@Id
//	@GenericGenerator(name = "alm_test_book_id", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "uuid")
	private UUID id;

	@Column(name = "title")
	@Length(max = 255)
	private String title;

	@ManyToOne
	@JoinColumn(name = "author_id")
	private Author author;

	@ManyToOne
	@JoinColumn(name = "serie_id")
	private Serie serie;

	@Override
	public boolean isNew() {
		return id == null;
	}
}
