package com.foreach.across.testmodules.springdata.extensions;

import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.testmodules.springdata.domain.SpringDataJpaModuleDomainConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@ModuleConfiguration(AcrossHibernateJpaModule.NAME)
@EntityScan(basePackageClasses = SpringDataJpaModuleDomainConfiguration.class)
class EntityScanConfiguration
{
}
