package com.foreach.across.testmodules.springdata.domain.user;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Table(name = "alm_test_user")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class User extends SettableIdAuditableEntity<User>
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_alm_test_user_id")
	@GenericGenerator(
			name = "seq_alm_test_user_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_alm_test_user_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Email
	@Column(name = "email")
	private String email;

	@Embedded
	private Address address;

	@Column(name = "date_of_birth")
	private LocalDate dateOfBirth;

	public String getName() {
		return StringUtils.join( firstName, " ", lastName );
	}
}
