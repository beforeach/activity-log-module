package com.foreach.across.testmodules.springdata.domain.order;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import com.foreach.across.testmodules.springdata.domain.book.Book;
import com.foreach.across.testmodules.springdata.domain.user.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Entity
@Table(name = "alm_test_order")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Order extends SettableIdAuditableEntity<Order>
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_alm_test_order_id")
	@GenericGenerator(
			name = "seq_alm_test_order_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_alm_test_order_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@Column(name = "order_date")
	private LocalDateTime orderDate;

	@ManyToMany
	@JoinTable(name = "alm_test_order_books",
			joinColumns = { @JoinColumn(name = "order_id", referencedColumnName = "id") },
			inverseJoinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"))
	private List<Book> books;

	@ManyToOne
	@JoinTable(name = "alm_test_order_user",
			joinColumns = { @JoinColumn(name = "order_id", referencedColumnName = "id") },
			inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	private User customer;
}
