package com.foreach.across.testmodules.springdata.domain.user;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@Embeddable
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Address
{
	@Column(name = "street")
	@Length(max = 255)
	private String street;

	@Column(name = "street_number")
	private Integer streetNumber;

	@Column(name = "zip_code")
	private Integer zipCode;

	@Column(name = "country")
	@Length(max = 255)
	private String country;
}
