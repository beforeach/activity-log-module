package com.foreach.across.samples.activitylog.application.extensions;

import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.samples.activitylog.application.domain.ActivityLogTestDomainConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@ModuleConfiguration(AcrossHibernateJpaModule.NAME)
@EntityScan(basePackageClasses = ActivityLogTestDomainConfiguration.class)
class EntityScanConfiguration
{
}
