package com.foreach.across.samples.activitylog;

import com.foreach.across.AcrossApplicationRunner;
import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.activitylog.ActivityLogModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.testmodules.springdata.SpringDataJpaModule;

/**
 * @author Steven Gentens
 * @since 0.0.1
 */
@AcrossApplication(
		modules = {
				ActivityLogModule.NAME,
				EntityModule.NAME,
				AdminWebModule.NAME,
				AcrossHibernateJpaModule.NAME,
				SpringDataJpaModule.NAME
		}, modulePackages = "com.foreach.across.testmodules"
)
public class ActivityLogModuleTestApplication
{
	public static void main( String[] args ) {
		AcrossApplicationRunner.run( ActivityLogModuleTestApplication.class, args );
	}
}
